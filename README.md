# hcp_test_retest

### Title: Robustness and reliability of whole-brain effective connectivity
### Project Lead: Stefan Frässle
### Supervising Prof.: Klaas Enno Stephan
### Abbreviation: HCP_TestRetest
### Date: November 17, 2020

This document contains the analysis plan of the project entitled “Robustness and reliability of whole-brain effective connectivity”. The project concerns the analysis of the robustness and test-retest reliability of whole-brain effective connectivity patterns, as inferred from functional magnetic resonance imaging (fMRI) data using regression dynamic causal modeling (rDCM). The project will make use of the test-retest data from the Human Connectome Project (HCP) dataset (Van Essen et al., 2013), which comprises resting-state and task-based fMRI data for two separate time points (sessions) from 45 healthy volunteers.
